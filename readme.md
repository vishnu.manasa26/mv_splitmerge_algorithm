Create file with all environment's packages
conda list --explicit
conda list --explicit > spec-file.txt

To use the spec file to create an identical environment on the same machine or another machine:
conda create --name myenv --file spec-file.txt

To use the spec file to install its listed packages into an existing environment:
conda install --name myenv --file spec-file.txt

Activation the environment on Mac:
source activate myenv


Source: https://conda.io/docs/user-guide/tasks/manage-environments.html


**To build local models:**
You can build the local models using the main.pyc file 
Results are of the algorithm can be viewed at location data/results/medical_data/multi_view_exp2

**Global model:**
Benchmark global model can be built using fca_benchmark.pyc
Created benchmark models are available at data/medical_data/multiview_exp2

Normal global model can be built using fca.pyc
Formal contexts and lattice are available at data/results/medical_data/multi_view_exp2/formal_context

**Evaluation of the results (calculates homogeneity, adjusted rand):**
Takes input from data/results/medical_data/multi_view_exp2/FinalClusterLabels (These CSV are obtained from Lattice using Excel)
It can be done using evaluation.pyc


If you want to look at the source code please contact me at vishnu.manasa.devagiri@bth.se
  